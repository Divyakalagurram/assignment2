package com.week2.assignment;
import java.util.*;

public class EmployeeDemo {

			public static void main(String[] args) {
				
				ArrayList<Employee> employees = new ArrayList<>();
	 
		        Employee emp1 = new Employee(); 
		        Employee emp2 = new Employee(); 
		        Employee emp3 = new Employee(); 
		        Employee emp4 = new Employee(); 
		        Employee emp5 = new Employee(); 
		      
		        emp1.setEmployeeDetails(1,"Aman",   20, 1100000,"    IT","   Delhi");
		        emp2.setEmployeeDetails(2,"Bobby  ",22, 500000,"     Hr   "," Bombay");
		        emp3.setEmployeeDetails(3,"Zoe",    20, 750000,"     Admin","  Delhi");
		        emp4.setEmployeeDetails(4,"Smitha ",21, 1000000,"    IT","     Chennai");
		        emp5.setEmployeeDetails(5,"Smitha", 24,  1200000,"   IT","    Bengaluru");
		       
		        
		       
		        employees.add(emp1);
		        employees.add(emp2);
		        employees.add(emp3);
		        employees.add(emp4);
		        employees.add(emp5);
		 
		       
		        System.out.println("Display Employee Details:");

		        emp1.getEmployeeDetails();
		        emp2.getEmployeeDetails();
		        emp3.getEmployeeDetails();
		        emp4.getEmployeeDetails();
		        emp5.getEmployeeDetails();
		       
		     	System.out.println("                              ");
		     	
		
				SortingEmployeeNames e= new SortingEmployeeNames();
				e.sortingNames(employees);
				System.out.println("                               ");
	
				CityCounting cityNameCount = new CityCounting();
				cityNameCount.cityCount(employees);
				System.out.println("                                ");
				MonthlySalary monthlySalary= new MonthlySalary();
				monthlySalary.monthlySalary(employees);
				System.out.println("                                ");
			}
		


	}
